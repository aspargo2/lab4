import json
import logging
import sys

import greengrasssdk

import awsiot.greengrasscoreipc
import awsiot.greengrasscoreipc.client as client
from awsiot.greengrasscoreipc.model import (
    QOS,
    PublishToIoTCoreRequest
)

# Logging
logger = logging.getLogger(__name__)
logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)

# SDK Client
#client = greengrasssdk.client("iot-data")


TIMEOUT = 10

ipc_client = awsiot.greengrasscoreipc.connect()

# Counter
my_counter = 0
def lambda_handler(event, context):
    global my_counter
    #TODO1: Get your data
    maxCO2Val = 0
    carId = 0
    
    for entry in event:
        carId = entry['vehicle_id']
        if entry["vehicle_CO2"] > maxCO2Val:
            maxCO2Val = entry["vehicle_CO2"]

    #TODO3: Return the result
    #client.publish(
    #    topic="maxco2/" + carId,
    #    payload=json.dumps(
    #        {"maxVal": maxCO2Val}
    #    ),
    #)
    topic = "maxco2/" + carId
    message = json.dumps({"maxVal": maxCO2Val})
    qos = QOS.AT_LEAST_ONCE

    request = PublishToIoTCoreRequest()
    request.topic_name = topic
    request.payload = bytes(message, "utf-8")
    request.qos = qos
    operation = ipc_client.new_publish_to_iot_core()
    operation.activate(request)
    future = operation.get_response()
    future.result(TIMEOUT)

    my_counter += 1

    return